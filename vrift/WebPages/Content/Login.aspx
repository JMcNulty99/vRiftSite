﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebPages/Master/vrift.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="vrift.WebPages.Content.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="server">
    <div class="imgholder">
        <img class="headerpic" width="1920" height="1080" src="../../Content/img/random/2.png" />
    </div>
    <div class="picturesbottom"></div>
    <div class="blocker">
        <div class="mainstuff">
            <form id="loginform" runat="server">
                <hr />
                <h3 style="color: black;">Log into your account!</h3>
                <hr />
                <div class="query">
                    Username<br />
                    Password<br />
                </div>
                <div class="input">
                    :&nbsp;<asp:TextBox ID="uname" runat="server"></asp:TextBox><br />
                    :&nbsp;<asp:TextBox ID="pass" runat="server"></asp:TextBox><br />
                </div>
                <asp:Button ID="loginbutton" runat="server" OnClick="addButton_Click" Text="Login" />
                <br />
                <hr />
                <h3 style="color: black;">Don't Have and account? Sign up now!</h3>
                <hr />
                <div class="query">
                    Username<br />
                    Password<br />
                    Email<br />
                    First Name<br />
                    Last Name<br />
                    Birthdate<br />
                    Phone<br />
                </div>
                <div class="input">
                    :&nbsp;<asp:TextBox ID="uname_s" runat="server"></asp:TextBox><br />
                    :&nbsp;<asp:TextBox ID="pass_s" runat="server"></asp:TextBox><br />
                    :&nbsp;<asp:TextBox AutoCompleteType="Email" ID="email" runat="server"></asp:TextBox><br />
                    :&nbsp;<asp:TextBox ID="fname" runat="server"></asp:TextBox><br />
                    :&nbsp;<asp:TextBox ID="lname" runat="server"></asp:TextBox><br />
                    :&nbsp;<asp:TextBox ID="birth" runat="server">(YYYY-MM-DD)</asp:TextBox><br />
                    :&nbsp;<asp:TextBox AutoCompleteType="Cellular" ID="phone" runat="server"></asp:TextBox><br />
                </div>
                <asp:Button ID="signupbutton" runat="server" OnClick="signButton_Click" Text="Signup Now" />
            </form>
            <hr />
            <br />
        </div>
    </div>
    <div class="mainbottom"></div>
</asp:Content>
