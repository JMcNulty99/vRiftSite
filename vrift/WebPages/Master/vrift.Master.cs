﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace vrift
{
    public partial class vrift : System.Web.UI.MasterPage
    {
        protected string accntname = "";
        protected string loggedin = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            //if we are logged into the website
            if (Session["LoggedIn"] != null && Session["LoggedIn"].ToString() == "logged")
            {
                account.Visible = true;
                account.InnerHtml = Session["acct"].ToString();
                login.Visible = false;
                loggedin = Session["LoggedIn"].ToString();
            }
            else
                account.Visible = false;

            if (Session["acct"] != null)
            {
                AcctN = Session["acct"].ToString();
            }
        }

        protected void account_event(object sender, EventArgs e)
        {
            Session["acct"] = AcctN;
            Session["LoggedIn"] = loggedin;
            Response.Redirect("Account.aspx");
        }

        string AcctN
        {
            get { return accntname; }
            set { accntname = value; }
        }
    }
}