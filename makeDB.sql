create database vuser;

use vuser;

create table phone(
	phoneid int primary key auto_increment,
	number char(10),
	phonetype char(4));
	
create table addr(
	addrid int primary key auto_increment,
	addrtype varchar(15),
	saddr1 varchar(30),
	saddr2 varchar(30),
	city	varchar(15),
	state char(2),
	zip char(5));

create table member(
	memberid int primary key auto_increment,
	fname varchar(15),
	lname varchar(15),
	uname varchar(15),
	email varchar(30),
	birthdate date,vusermember
	signupdate timestamp default current_timestamp(),
	phoneid int,
	addrid int,
	foreign key (phoneid) references phone(phoneid),
	foreign key (addrid) references addr(addrid));